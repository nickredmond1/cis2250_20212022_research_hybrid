import React, { Component } from 'react';
import { Text, TextInput, Button, View } from 'react-native';

const price = 10;
const numberOfTickets = 5;
export default class TicketCostCalculator extends Component {
    constructor(props) {
        super(props);
        this.state = {
            numberOfTickets: parseInt(0, 10),
            ticketPrice: parseInt(0, 8),
            totalCost: parseInt(0, 10)
        };

    }


    calculate = (input) => {
        this.setState({ numberOfTickets: input })
        this.setState({ totalCost: (this.numberOfTickets * this.ticketPrice) })
    }
    render() {
        return (
            <View style={{ padding: 10 }}>
                <Text>Number of tickets : {this.state.numberOfTickets}
                    Price per ticket: {this.state.ticketPrice}
                    Total cost: {this.state.totalCost}
                </Text>
                <TextInput
                    placeholder="Enter number of tickets"
                    keyboardType='numeric'
                    value={this.state.numberOfTickets}
                    onChangeText={input => this.calculate(parseInt(input, 10))}
                >

                </TextInput>
            </View>
        )

    }

}
